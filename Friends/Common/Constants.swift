//
//  Constants.swift
//  Friends
//
//  Created by Fahad Hasan Zahidi on 5/23/21.
//

import Foundation
import UIKit

struct Constants {
    static let SCREEN_RATIO_RESPECT_OF_IPHONE_6P = min(UIScreen.main.bounds.height, UIScreen.main.bounds.width) / 414.0
    static let home_cell_height: CGFloat = 168
    static let first_element: Int = 0
    static let selected_profile_key: String = "selected_random_user"
}
