//
//  File.swift
//  Friends
//
//  Created by Fahad Hasan Zahidi on 5/23/21.
//

import Foundation

struct RandomUserInfo: Codable {
    var results: [UserInfo]
}

struct UserInfo: Codable {
    var name: UserName
    var location: UserLocation
    var email: String
    var phone: String
    var picture: Picture
}

struct UserName: Codable {
    var title: String
    var first: String
    var last: String
}

struct UserLocation: Codable {
    var street: UserStreet
    var city: String
    var state: String
    var country: String
    var postcode: Int
    var coordinates: UserCoordinatesLatest
    var timezone: UserTimeZoneLatest
    
}

struct UserStreet: Codable {
    var number: Int
    var name: String
}

struct UserCoordinatesLatest: Codable {
    var latitude: String
    var longitude: String
}

struct UserTimeZoneLatest: Codable {
    var offset: String
    var description: String
}

struct Picture: Codable {
    var large: String
    var medium: String
    var thumbnail: String
}

class UserDataManager {
    static var randomDataCollection: [RandomUserInfo] = []
    
    func requestForNewData() {
        print("requestForNewData")
        let urlString = "https://randomuser.me/api/"
        
        for _ in 1...10 {
            if let url = URL(string: urlString) {
                if let data = try? Data(contentsOf: url) {
                    //print(String(data: data, encoding: .utf8)!)
                    parse(json: data)
                }
            }
        }
    }
    
    fileprivate func parse(json: Data) {
        let decoder = JSONDecoder()
        
        if let jsonResultsData = try? decoder.decode(RandomUserInfo.self, from: json) {
            //print(jsonResultsData.results)
            UserDataManager.randomDataCollection.append(jsonResultsData)
            
        }
    }
    
}
