//
//  CustomNavigationController.swift
//  Friends
//
//  Created by Fahad Hasan Zahidi on 5/23/21.
//

import Foundation
import UIKit

class CustomNavigationController :UINavigationController {

        override func viewDidLoad() {
            super.viewDidLoad()
        }

        override var preferredStatusBarStyle: UIStatusBarStyle{
               if let topView = viewControllers.last{
                   return topView.preferredStatusBarStyle
               }
               return  UIStatusBarStyle.lightContent
        }
        
        override func didReceiveMemoryWarning() {
            super.didReceiveMemoryWarning()
        }
}
