//
//  ViewManager.swift
//  Friends
//
//  Created by Fahad Hasan Zahidi on 5/23/21.
//

import Foundation
import UIKit

var viewManager: CustomViewManager?

class CustomViewManager {
    fileprivate let TAG: String = "ViewManager"

    fileprivate var window: UIWindow?
    
    fileprivate static var navigationController: CustomNavigationController?

    init(_ window: UIWindow?) {
        if(window == nil){
            self.window = UIWindow(frame:UIScreen.main.bounds)
        }
        else{
            self.window = window
        }
        let homeView: UIViewController = HomeViewController(nibName: "HomeViewController", bundle: nil)
        CustomViewManager.navigationController = CustomNavigationController.init(rootViewController: homeView)
        self.window?.rootViewController = CustomViewManager.navigationController
        self.window?.makeKeyAndVisible()
    }
    
    static func getNavigationController()->CustomNavigationController?{
        return CustomViewManager.navigationController
    }

}
