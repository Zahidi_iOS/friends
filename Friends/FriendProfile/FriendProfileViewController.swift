//
//  FriendProfileViewController.swift
//  Friends
//
//  Created by Fahad Hasan Zahidi on 5/23/21.
//

import UIKit

class FriendProfileViewController: UIViewController {

    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var address: UILabel!
    @IBOutlet weak var city: UILabel!
    @IBOutlet weak var mail: UILabel!
    @IBOutlet weak var phone: UILabel!
    
    var selected_index: Int = 0
    var profileData: UserInfo = UserDataManager.randomDataCollection[0].results[Constants.first_element]
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        getProfileData()
        addTapGesture()
        updateUI()
    }
    
    fileprivate func getProfileData() {
        selected_index = UserDefaults.standard.integer(forKey: Constants.selected_profile_key)
        profileData = UserDataManager.randomDataCollection[selected_index].results[Constants.first_element]
    }
    
    fileprivate func addTapGesture() {
        let tap = UITapGestureRecognizer(target: self, action: #selector(openMailApp))
        mail.addGestureRecognizer(tap)
    }
    
    fileprivate func updateUI () {
        self.title = "Profile"
        let urlString = (profileData.picture.large)
        let url = URL(string: urlString)!
        let data = try? Data(contentsOf: url)
        imageView.image = UIImage(data: data!)
        name.text = getNameOfUser(userName: profileData.name)
        address.text = getUserAddress()
        city.text = profileData.location.city
        mail.text = profileData.email
        phone.text = profileData.phone
    }

    @objc fileprivate func openMailApp() {
        let email: String = profileData.email
        print(email)
        if let url = URL(string: "mailto:\(email)") {
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(url)
            }
            else {
                UIApplication.shared.openURL(url)
            }
        }
    }
    
    fileprivate func getNameOfUser(userName: UserName) -> String {
        var name = ""
        name = userName.title + ". " + userName.first + " " + userName.last
        return name
    }
    
    fileprivate func getUserAddress() -> String {
        
        return String((profileData.location.street.number)) + " " + (profileData.location.street.name)
    }

}
