//
//  FriendCardCell.swift
//  Friends
//
//  Created by Fahad Hasan Zahidi on 5/23/21.
//

import UIKit

class FriendCardCell: UICollectionViewCell {
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var country: UILabel!
    
    @IBOutlet weak var imageHeight: NSLayoutConstraint!
    @IBOutlet weak var imageLeading: NSLayoutConstraint!
    @IBOutlet weak var imageTrailing: NSLayoutConstraint!
    @IBOutlet weak var nameTop: NSLayoutConstraint!

    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        updateUI()
    }
    
    fileprivate func updateUI() {
        imageHeight.constant = 100 * Constants.SCREEN_RATIO_RESPECT_OF_IPHONE_6P
        imageLeading.constant = 50 * Constants.SCREEN_RATIO_RESPECT_OF_IPHONE_6P
        imageTrailing.constant = 50 * Constants.SCREEN_RATIO_RESPECT_OF_IPHONE_6P
        nameTop.constant = 5 * Constants.SCREEN_RATIO_RESPECT_OF_IPHONE_6P
    }
    
    static var loadNib:UINib {
        return UINib.init(nibName: String(describing: FriendCardCell.classForCoder()), bundle: nil)
    }
    
    static var reuseIdentifier:String {
        return String(describing: FriendCardCell.classForCoder())
    }

}
