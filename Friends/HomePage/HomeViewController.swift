//
//  HomeViewController.swift
//  Friends
//
//  Created by Fahad Hasan Zahidi on 5/23/21.
//

import UIKit

class HomeViewController: UIViewController {
    
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var collectionView: UICollectionView!
    
    fileprivate let cellReuseIdentifier: String = "FrinedHomeCustomCell"
    fileprivate var menuList: [[RandomUserInfo]] = []
    fileprivate var timer: Timer = Timer()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        updateUI()
        configureCollectionView()
        createMenuList()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        updateUI()
    }
    
    fileprivate func updateUI() {
        self.title = "Friends Home"
        collectionView.isHidden = true
        timer = Timer.scheduledTimer(timeInterval: 5, target: self, selector: #selector(timerDidEnd), userInfo: nil, repeats: false)
        activityIndicator.startAnimating()
        //collectionView.reloadData()
    }
    
    fileprivate func configureCollectionView() {
        collectionView.dataSource = self
        collectionView.delegate = self
        collectionView.register(FriendCardCell.loadNib, forCellWithReuseIdentifier: FriendCardCell.reuseIdentifier)
    }
    
    fileprivate func createMenuList() {
        menuList.append(UserDataManager.randomDataCollection)
    }
    
    //demo purpose
    @objc fileprivate func timerDidEnd() {
        if (!UserDataManager.randomDataCollection.isEmpty) {
            dataGenerated()
        }
        else {
            let alert = UIAlertController.init(title: "Poor Internet connection", message: "Please check your internet connection", preferredStyle: .alert)
            let okAction = UIAlertAction.init(title: "OK", style: .default, handler: nil)
            alert.addAction(okAction)
            self.navigationController?.present(alert, animated: true, completion: nil)
            activityIndicator.stopAnimating()
            activityIndicator.isHidden = true
        }
    }
    
    fileprivate func dataGenerated() {
        self.collectionView.reloadData();
        collectionView.isHidden = false
        activityIndicator.stopAnimating()
        activityIndicator.isHidden = true
    }
    
    fileprivate func getNameOfUser(userName: UserName) -> String {
        var name = ""
        name = userName.title + ". " + userName.first + " " + userName.last
        return name
    }

}

extension HomeViewController: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return menuList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return menuList[section].count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: FriendCardCell.reuseIdentifier, for: indexPath) as! FriendCardCell
        let menuData = menuList[indexPath.section][indexPath.row].results[Constants.first_element]
        cell.name.text = getNameOfUser(userName: menuData.name)
        cell.country.text = menuData.location.country
        let url = URL(string: menuData.picture.medium)!
        let data = try? Data(contentsOf: url)
        cell.imageView.image = UIImage(data: data!)
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        UserDefaults.standard.setValue(indexPath.row, forKey: Constants.selected_profile_key)
        let profileView = FriendProfileViewController.init(nibName: "FriendProfileViewController", bundle: nil)
              CustomViewManager.getNavigationController()?.pushViewController(profileView, animated: true)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = collectionView.bounds.width/2
        let height = width
        return CGSize(width: width, height: height)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets.zero
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
}
