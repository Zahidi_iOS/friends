//
//  TableViewCell.swift
//  Friends
//
//  Created by Fahad Hasan Zahidi on 5/23/21.
//

import UIKit

class HomeSingleLineCell: UITableViewCell {

    @IBOutlet weak var imagePortrait: UIImageView!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var country: UILabel!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    static var loadNib:UINib {
        return UINib.init(nibName: String(describing: HomeSingleLineCell.classForCoder()), bundle: nil)
    }
    
    static var reuseIdentifier:String {
        return String(describing: HomeSingleLineCell.classForCoder())
    }
    
}
